require('dotenv').config()
const express=require("express")
const cors=require("cors")
const cookieParser=require("cookie-parser")
const { log } = require("console")
const mongoose=require('mongoose')
const router = require('./routes')
const errorMiddleware = require('./middleware/error-middleware')

const PORT=process.env.PORT || 7777
const app=  express()

app.use(express.json())
app.use(cookieParser())
app.use(cors())
app.use("/api",router)
app.use(errorMiddleware)
const start=async ()=>{
    try {
await mongoose.connect(process.env.URL)
         app.listen(PORT,()=>log(`Server started on Port = ${PORT}`))
    } catch (error) {
        console.log(error);
    }
}
start()