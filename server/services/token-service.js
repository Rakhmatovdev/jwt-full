const jwt = require("jsonwebtoken");
const tokenModel = require("../models/token-model");

class TokenService {
  generationTokens(payload) {
    const accessToken = jwt.sign(payload, process.env.JWT_ACCESS_SECRET, {
      expiresIn: "26m",
    });
    const refreshToken = jwt.sign(payload, process.env.JWT_REFRESH_SECRET, {
      expiresIn: "26d",
    });

    return {
      accessToken,
      refreshToken,
    };
  }

  validationAccessToken() {
    try {
      const userData=jwt.verify(token,process.env.JWT_ACCESS_SECRET)
      return userData
    } catch (error) {
      return null;
    }
  }

  validationRefreshToken() {
    try {
      const userData=jwt.verify(token,process.env.JWT_REFRESH_SECRET)
      return userData
    } catch (error) {
      return null
    }
  }

  async saveToken(userId, refreshToken) {
    const tokenDate = await tokenModel.findOne({ user: userId });
    if (tokenDate) {
      tokenDate.refreshToken = refreshToken;
      return tokenDate.save();
    }
    const token = await tokenModel.create({ user: userId, refreshToken });
    return token;
  }

  async removeToken(refreshToken) {
    const tokenDate = await tokenModel.deleteOne({ refreshToken });
    return tokenDate;
  }
  async findToken(refreshToken) {
    const tokenDate = await tokenModel.findOne({ refreshToken });
    return tokenDate;
  }
}

module.exports = new TokenService();
