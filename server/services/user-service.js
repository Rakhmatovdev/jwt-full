const userModel = require("../models/user-model");
const bcrypt = require("bcrypt");
const { v4: uuid } = require("uuid");
const mailService = require("./mail-service");
const tokenService = require("./token-service");
const UserDto = require("../dtos/user-dto");
const apiError = require("../exeptions/api-error");

class UserService {
  async registration(email, password) {
    const condidate = await userModel.findOne({ email });
    if (condidate) {
      throw apiError.BadRequestError(
        `Bu emailga ega foydalanuvchi ${email} allaqachon mavjud`
      );
    }

    const activationLink = uuid();
    const hashPassword = await bcrypt.hash(password, 3);
    const user = await userModel.create({
      email,
      password: hashPassword,
      activationLink,
    });
    await mailService.sendActivationMail(
      email,
      `${process.env.API_URL}/api/activate/${activationLink}`
    );

    const userDto = new UserDto(user); //id,email,isActivated
    const tokens = tokenService.generationTokens({ ...userDto });
    await tokenService.saveToken(userDto.id, tokens.refreshToken);

    return {
      ...tokens,
      user: userDto,
    };
  }
  async activate(activationLink) {
    const user = await userModel.findOne({ activationLink });
    if (!user) {
      throw apiError.BadRequestError("Link no yet");
    }
    user.isActivated = true;
    await user.save();
  }

  async login(email, password) {
    const user = await userModel.findOne({ email });
    if (!user) {
      throw apiError.BadRequestError("User is no jasurinc");
    }
    const isPassEquials = await bcrypt.compare(password, user.password);
    if (!isPassEquials) {
      throw apiError.BadRequestError("Password is no equial");
    }
    const userDto = new UserDto(user);
    const tokens = tokenService.generationTokens({ ...userDto });
    await tokenService.saveToken(userDto.id, tokens.refreshToken);

    return {
      ...tokens,
      user: userDto,
    };
  }
  async logout(refreshToken) {
    const token = await tokenService.removeToken(refreshToken);
    return token;
  }
  async refresh(refreshToken) {
    if (!refreshToken) {
      throw apiError.UnauthorizedError();
    }
    const userData= tokenService.validationAccessToken(refreshToken)
    const tokenFromDb=await tokenService.findToken(refreshToken)
    if (!userData || !tokenFromDb) {
      throw apiError.UnauthorizedError();
    }
    const user= await userModel.findById(userData.id)
    const userDto = new UserDto(user);
    const tokens = tokenService.generationTokens({ ...userDto });
    await tokenService.saveToken(userDto.id, tokens.refreshToken);

    return {
      ...tokens,
      user: userDto,
    };
  }
   async getAllUsers(){
    const users= await userModel.find()
    return users
   }
}

module.exports = new UserService();
